'use strict';

import React from 'react';
import { Link } from 'react-router';
import NotFoundPage from './NotFoundPage';
import ProductTestimonials from './ProductTestimonials';
import products from '../data/products-list';

export default class ProductPage extends React.Component {
  render() {
    const id = this.props.params.id;
    const product = products.filter((product) => product.id == id)[0];
    if (!product) {
      return <NotFoundPage/>;
    }
    return (
      <div className="product-full">
        <div className="product">
          <div className="picture-container">
            <img src={`${product.imageLink}`}/>
            <h2 className="name">{product.name}</h2>
            <h3 className="price">Rs. {product.price}</h3>
          </div>
          <section className="description">
            {product.description}
          </section>
          <section className="testimonials">
            <p>Few testimonials from buyers,</p>
            <ul>
              {product.testimonials.map((testimonial, i) => <ProductTestimonials key={i} {...testimonial} /> )}
            </ul>
          </section>
        </div>
        <div className="navigateBack">
          <Link to="/">« Back to the main page</Link>
        </div>
      </div>
    );
  }
}
