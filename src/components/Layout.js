'use strict';

import React from 'react';
import { Link } from 'react-router';
import Header from './Header';
import Footer from './Footer';

export default class Layout extends React.Component {
	constructor() {
	  super();

	  this.state = {};
	}
  render() {
    return (
      <div className="app-container">
        <Header />
        
        <div className="app-content">{this.props.children}</div>
        
        <Footer />
      </div>
    );
  }
}
