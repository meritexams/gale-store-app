import React from 'react'

let Footer = function(props) {
  return (

<footer className="page-footer">
    <div className="footer-copyright">
      <div className="container">
      Made by Materialize
      </div>
    </div>
  </footer>
  )
}

export default Footer
