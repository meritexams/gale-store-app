'use strict';

import React from 'react';
import Search from './Search';

export default class IndexPage extends React.Component {
  render() {
    return (
      <div className="home">
        <Search />
      </div>
    );
  }
}
