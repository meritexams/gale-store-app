import React from 'react'

export default class Header extends React.Component {
  constructor(props) {
    super(props);  
    this.state = {};
  }
  render() {
    return (
      <nav className="light-blue lighten-1" role="navigation" >
      <div className="nav-wrapper container center-align">
        <a id="logo-container" href="#" className="brand-logo">Logo</a>
        <ul className="right hide-on-med-and-down">
          <li><i className="large material-icons">star</i></li>
          <li><a href="#">Login</a></li>
        </ul>

        <ul id="nav-mobile" className="side-nav">
          <li><a href="#">Navbar Link</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" className="button-collapse"><i className="material-icons">menu</i></a>
      </div>
    </nav>
    )
  }
}

