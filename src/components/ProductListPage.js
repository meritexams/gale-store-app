'use strict';

import React from 'react';
import products from '../data/products-list';
import ProductListView from './ProductsListView';
import NotFoundPage from './NotFoundPage';

export default class ProductListPage extends React.Component {
  
  constructor(props) {
    super(props);  
    this.state = {starred: []};
    this.handleStar = this.handleStar.bind(this);
  }

  arrPushNotExists(elem, arr) {
      var index = arr.indexOf(elem);
      if(index >= 0)
        arr.splice(index, 1);
      else
        arr.push(elem); 
      return arr;
    }

  handleStar(productId) {
    this.setState({
       starred: this.arrPushNotExists(productId, this.state.starred)
      });
  }
  render() {
    const category = this.props.params.category;
    const product = products.filter((product) => 
      category.indexOf(product.category) !== -1
    );

    if (!product.length) {
      return <NotFoundPage/>;
    }
    return (
      <div className="products row">
        {product.map((item, i) => <ProductListView key={i} favourites={this.state.starred} handleStar={this.handleStar} {...item} /> ) }
      </div>
    );
  }
}
