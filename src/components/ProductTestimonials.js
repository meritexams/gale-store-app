'use strict';

import React from 'react';
import { Link } from 'react-router';

export default class ProductTestimonials extends React.Component {
  
  render() {
    return (
        <li>{this.props.message} -- by {this.props.guestName}</li>
    );
  }
}
