'use strict';

import React from 'react';
import { Link } from 'react-router';

export default class ProductListView extends React.Component {
  constructor(props) {
     super(props);
   }

  localHandleStar(id) {
    this.props.handleStar(id);
  }
  render() {
    return (
      <div className="product-preview col s12 m4 4">
          
          <div className="card horizontal">
            <div className="card-image">
              <img src={this.props.imageLink} />
            </div>
            <div className="card-stacked">
              <div className="card-content">
              <h2 className="header">
            <Link to={`/product/${this.props.id}`}>{this.props.name}</Link>
          </h2>
                <p>Location: {this.props.location}</p>
              </div>
              <div className="card-action">
                <p>Price: {this.props.price}</p>
                <p className="right-align">
                  <i onClick={this.localHandleStar.bind(this, this.props.id)} className={(this.props.favourites.find( (favId) => favId == this.props.id))?'starred productfav small material-icons':'productfav small material-icons'} >star</i>
                </p>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
