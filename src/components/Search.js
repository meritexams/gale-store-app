import React from 'react'

let Search = function(props) {
  return (
      <main>
        <div className="container">
          <div className="row searchpage">
            <h2>Search your product</h2>
            <div className="col s8">

              <div className="row">
                <div className="input-field col s12">
                    <div className="autocomplete" id="multiple">
                        <div className="ac-users"></div>
                        <div className="ac-input">
                            <input type="text" id="search-product" placeholder="type to search" data-activates="multipleDropdown" data-beloworigin="true" autocomplete="off" />
                        </div>
                        <ul id="multipleDropdown" className="dropdown-content ac-dropdown"></ul>
                        <input type="hidden" name="multipleHidden" />
                    </div>
                </div>
            </div>

            </div>
            <div className="col s4">
              <button onclick="search();" className="btn waves-effect waves-light" id="btnSearch">Search
                <i className="material-icons right">search</i>
              </button>
            </div>
          </div>
        </div>
      </main>
  )
}

export default Search
