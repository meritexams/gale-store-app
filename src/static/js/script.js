$(function() {
    $('.carousel').carousel({full_width: true});
    setInterval(function(){
            $('.carousel').carousel('next');
      }, 4000);

    var autocomplete = $('#search-product').materialize_autocomplete({
        limit: 20,
        multiple: {
            enable: true,
            maxSize: 3
        },
        appender: {
            el: '.ac-users'
        },
        dropdown: {
            el: '#multipleDropdown'
        },
        getData: function (value, callback) {
            console.log(value);
            //callback(value, data);
        }
    });
var resultCache = {
            'A': [
                {
                    id: 'Abe',
                    text: 'Abe'
                },
                {
                    id: 'Ari',
                    text: 'Ari'
                }
            ],
            'B': [
                {
                    id: 'Baz',
                    text: 'Baz'
                }
            ],
            'BA': [
                {
                    id: 'Baz',
                    text: 'Baz'
                }
            ],
            'BAZ': [
                {
                    id: 'Baz',
                    text: 'Baz'
                }
            ],
            'AB': [
                {
                    id: 'Abe',
                    text: 'Abe'
                }
            ],
            'ABE': [
                {
                    id: 'Abe',
                    text: 'Abe'
                }
            ],
            'AR': [
                {
                    id: 'Ari',
                    text: 'Ari'
                }
            ],
            'ARI': [
                {
                    id: 'Ari',
                    text: 'Ari'
                }
            ]
        };

        autocomplete.resultCache = resultCache;
        
    $("#btnSearch").click(function(){
        console.dir(autocomplete.value);
    });
});
